###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "This forms the name of the SQS queue. It is prefixed to '.fifo' if 'fifo' is 'true'."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

###############################################################################
# Optional Variables
###############################################################################

variable "fifo" {
  type        = bool
  default     = false
  description = "If 'true' designates a FIFO (First-In-First-Out) queue."
}

variable "fifo_deduplication" {
  type        = bool
  default     = false
  description = "Enables content-based deduplication for FIFO queues. 'fifo' must be 'true'."
}

###############################################################################

variable "delay_seconds" {
  type        = number
  default     = 0
  description = "The time in seconds that the delivery of all messages in the queue will be delayed."
}

variable "message_size_bytes" {
  type        = number
  default     = 262144
  description = "The limit of how many bytes a message can contain before Amazon SQS rejects it."
}

variable "receive_wait_seconds" {
  type        = number
  default     = 0
  description = "The time for which a ReceiveMessage call will wait for a message to arrive (long polling) before returning."
}

variable "message_retention_seconds" {
  type        = number
  default     = 172800
  description = "The number of seconds Amazon SQS retains a message."
}

variable "visibility_timeout_seconds" {
  type        = number
  default     = 120
  description = "The visibility timeout for the queue in seconds."
}

###############################################################################

variable "queue_policy" {
  type        = string
  default     = null
  description = "The JSON policy for the SQS queue."
}

variable "redrive_policy" {
  type        = string
  default     = null
  description = "The JSON policy to set up the dead letter queue."
}

###############################################################################

variable "kms_key_id" {
  type        = string
  default     = null
  description = "The ID of an AWS-managed customer master key (CMK) for Amazon SQS or a custom CMK."
}

variable "kms_reuse_seconds" {
  type        = number
  default     = 300
  description = "The length of time, in seconds, for which Amazon SQS can reuse a data key to encrypt or decrypt messages before calling AWS KMS again."
}

###############################################################################
# Locals
###############################################################################

locals {
  name               = format("%s%s", var.class, var.fifo ? ".fifo" : "")
  fifo_deduplication = var.fifo ? var.fifo_deduplication : false
}

###############################################################################
# Resources
###############################################################################

resource "aws_sqs_queue" "scope" {
  name                              = local.name
  policy                            = var.queue_policy
  fifo_queue                        = var.fifo
  delay_seconds                     = var.delay_seconds
  redrive_policy                    = var.redrive_policy
  max_message_size                  = var.message_size_bytes
  kms_master_key_id                 = var.kms_key_id
  message_retention_seconds         = var.message_retention_seconds
  receive_wait_time_seconds         = var.receive_wait_seconds
  visibility_timeout_seconds        = var.visibility_timeout_seconds
  content_based_deduplication       = local.fifo_deduplication
  kms_data_key_reuse_period_seconds = var.kms_reuse_seconds

  tags = {
    FiFo    = var.fifo
    Name    = local.name
    Class   = var.class
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

###############################################################################

output "fifo" {
  value = var.fifo
}

output "fifo_deduplication" {
  value = local.fifo_deduplication
}

###############################################################################

output "delay_seconds" {
  value = var.delay_seconds
}

output "message_size_bytes" {
  value = var.message_size_bytes
}

output "receive_wait_seconds" {
  value = var.receive_wait_seconds
}

output "message_retention_seconds" {
  value = var.message_retention_seconds
}

output "visibility_timeout_seconds" {
  value = var.visibility_timeout_seconds
}

###############################################################################

output "queue_policy" {
  value = var.queue_policy
}

output "redrive_policy" {
  value = var.redrive_policy
}

###############################################################################

output "kms_key_id" {
  value = var.kms_key_id
}

output "kms_reuse_seconds" {
  value = var.kms_reuse_seconds
}

###############################################################################

output "name" {
  value = local.name
}

###############################################################################

output "id" {
  value = aws_sqs_queue.scope.id
}

output "arn" {
  value = aws_sqs_queue.scope.arn
}

###############################################################################
