<!---------------------------------------------------------------------------->

# sqs/queue

#### Provides a Simple Queue Services ([SQS]) queue

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/queue/aws//sqs/queue`**

-------------------------------------------------------------------------------

### Example Usage

```
module "sqs_hotfolder" {
  source  = "gitlab.com/bitservices/queue/aws//sqs/queue"
  class   = "foobar"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!---------------------------------------------------------------------------->

[SQS]: https://aws.amazon.com/sqs/

<!---------------------------------------------------------------------------->
